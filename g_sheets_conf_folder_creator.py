from __future__ import print_function
import pickle
import os.path
import os
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import operator

#set the base path of where you would like this to generate
base_path = '/Users/conf/Documents/NWRA2020/'

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly']

# The ID and range of a sample spreadsheet.
SPREADSHEET_ID = '1jVyM_ybfNxoTv9dFTc3YBCA8nBo5IQTSqR5FPUx0KAc'
RANGE_NAME = 'Test!A2:F'

def main():
    """Shows basic usage of the Sheets API.
    Prints values from a sample spreadsheet.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds)

    # Call the Sheets API
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=SPREADSHEET_ID,
                                range=RANGE_NAME).execute()
    values = result.get('values', [])
    
    day_cur = ''
    day_vals = {}
    day_count = 0
    day_room_cur = ''
    day_room_vals = {}
    day_room_count = 0
    day_room_pres_cur = ''
    day_room_pres_vals = {}
    day_room_pres_count = 0
    breaks = ["lunch", "dinner", "poster break", "night off", "done!"]
    
    if not values:
        print('No data found.')
    else:
        for row in values:
           
            #row[0] - day
            #row[1] - room
            #row[3] - title
            #row[4] - live stream presenter

            #if title isn't a break then proceed
            if row[3].lower() not in breaks:
                #set values from value index
                day_cur = row[0]
                day_room_cur = row[0] + row[1]
                room = row[1]

                #because values are only returned up to the last filled column in the range
                #the row may not have an index that goes up to live stream presenter
                #if that's the case then set the presenter to the title
                if len(row) < 5:
                    presenter = row[3]
                else:
                    presenter = row[4]

                day_room_pres_cur = day_cur + day_room_cur + presenter

                #assign a number to the days based on the order they're encountered on the sheet
                #only do if the day is not already recorded
                if day_cur not in day_vals.keys():
                    day_count = day_count + 1
                    day_vals[day_cur] = str(day_count).rjust(2, '0') + '-' + day_cur
                    day_room_count = 0

                #assign a number to the day/room combo on the order they are in the sheet
                if day_room_cur not in day_room_vals.keys():
                    day_room_count = day_room_count + 1
                    day_room_vals[day_room_cur] = str(day_room_count).rjust(2, '0') + '-' + room
                    day_room_pres_vals[day_room_cur] = 0

                #assign a number to the presenter based on the day/room and how they are in the sheet
                if day_room_pres_cur not in day_room_pres_vals.keys():
                    day_room_pres_vals[day_room_cur] = day_room_pres_vals[day_room_cur] + 1
                    day_room_pres_count = day_room_pres_count + 1
                    day_room_pres_vals[day_room_pres_cur] = str(day_room_pres_vals[day_room_cur]).rjust(2, '0') + '-' + presenter

                formatted_day = day_vals[day_cur]
                formatted_room = day_room_vals[day_room_cur]
                formatted_presenter = day_room_pres_vals[day_room_pres_cur]
                #construct a file path
                for i in ["Presentation", "Livestream"]:
                    pres_dir_path = "%s/%s/%s/%s" % (i, formatted_day, formatted_room, formatted_presenter)
                    #print(pres_dir_path)
                    #create a directory with the path if it doesn't already exist
                    create_directory(pres_dir_path)

                for i in ["Podium", "Board"]:
                    vr_dir_path = "VoiceRecorder/%s/%s/%s" % (formatted_day, formatted_room, i)
                    #print("\t", vr_dir_path)
                    create_directory(vr_dir_path)

                for i in ["Room", "Podium"]:
                    cam_dir_path = "Camera/%s/%s/%s" % (formatted_day, formatted_room, i)
                    #print("\t", cam_dir_path)
                    create_directory(cam_dir_path)

def create_directory(dir_path):
    full_path = base_path + dir_path
    if not os.path.exists(full_path):
        os.makedirs(full_path)
        print("Directory " , full_path ,  " Created ")
    else:    
        print("Directory " , full_path ,  " already exists") 

if __name__ == '__main__':
    main()
